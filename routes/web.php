<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*  Tracking Company route */
Route::group(['prefix' => '/trackings'], function(){
	Route::get('/', 'TrackingController@index')->name('tracking.index');
	Route::get('/create', 'TrackingController@create')->name('tracking.create');
	Route::post('/store', 'TrackingController@store')->name('tracking.store');
	Route::get('/edit/{id}', 'TrackingController@edit')->name('tracking.edit');
	Route::post('/update/{id}', 'TrackingController@update')->name('tracking.update');
	Route::get('/delete/{id}', 'TrackingController@delete')->name('tracking.delete');
});
/* Tracking Company route end */

/*  Drivers route */
Route::group(['prefix' => '/drivers'], function(){
	Route::get('/', 'DriverController@index')->name('driver.index');
	Route::get('/create', 'DriverController@create')->name('driver.create');
	Route::post('/store', 'DriverController@store')->name('driver.store');
	Route::get('/edit/{id}', 'DriverController@edit')->name('driver.edit');
	Route::post('/update/{id}', 'DriverController@update')->name('driver.update');
	Route::get('/delete/{id}', 'DriverController@delete')->name('driver.delete');
});
/* Drivers route end */

/*  Plates route */
Route::group(['prefix' => '/plates'], function(){
	Route::get('/', 'PlateController@index')->name('plate.index');
	Route::get('/create', 'PlateController@create')->name('plate.create');
	Route::post('/store', 'PlateController@store')->name('plate.store');
	Route::get('/edit/{id}', 'PlateController@edit')->name('plate.edit');
	Route::post('/update/{id}', 'PlateController@update')->name('plate.update');
	Route::get('/delete/{id}', 'PlateController@delete')->name('plate.delete');
});
/* Plates route end */

/*  So Number route */
Route::group(['prefix' => '/sonumbers'], function(){
	Route::get('/', 'SOController@index')->name('sonumber.index');
	Route::get('/create', 'SOController@create')->name('sonumber.create');
	Route::post('/store', 'SOController@store')->name('sonumber.store');
	Route::get('/edit/{id}', 'SOController@edit')->name('sonumber.edit');
	Route::post('/update/{id}', 'SOController@update')->name('sonumber.update');
	Route::get('/delete/{id}', 'SOController@delete')->name('sonumber.delete');
	Route::get('/paid/{id}', 'SOController@paid')->name('sonumber.paid');
	Route::get('/unpaid/{id}', 'SOController@unpaid')->name('sonumber.unpaid');
	Route::get('/change/{id}', 'SOController@change_status')->name('sonumber.change');
	Route::post('/for_payment', 'SOController@for_payment')->name('sonumber.for_payment');
});
	Route::post('/sonumber/get_driver','SOController@get_driver');
	Route::post('/sonumber/get_plate','SOController@get_plate');
/* So Number route end */

/*  Load route */
Route::group(['prefix' => '/loads'], function(){
	Route::get('/', 'LoadController@index')->name('load.index');
	Route::get('/create', 'LoadController@create')->name('load.create');
	Route::post('/store', 'LoadController@store')->name('load.store');
	Route::get('/edit/{id}', 'LoadController@edit')->name('load.edit');
	Route::post('/update/{id}', 'LoadController@update')->name('load.update');
	Route::get('/delete/{id}', 'LoadController@delete')->name('load.delete');
});
/* Load route end */

/*  User route */
Route::group(['prefix' => '/users'], function(){
	Route::get('/', 'MyUserController@index')->name('user.index');
	Route::get('/create', 'MyUserController@create')->name('user.create');
	Route::post('/store', 'MyUserController@store')->name('user.store');
	Route::get('/edit/{id}', 'MyUserController@edit')->name('user.edit');
	Route::post('/update/{id}', 'MyUserController@update')->name('user.update');
	Route::get('/delete/{id}', 'MyUserController@delete')->name('user.delete');

	Route::get('/editacc/{id}', 'MyUserController@editacc')->name('user.editacc');
	Route::post('/updateacc/{id}', 'MyUserController@updateacc')->name('user.updateacc');
});
/* User route end *

/*  Client route */
Route::group(['prefix' => '/clients'], function(){
	Route::get('/', 'ClientController@index')->name('client.index');
	Route::get('/create', 'ClientController@create')->name('client.create');
	Route::post('/store', 'ClientController@store')->name('client.store');
	Route::get('/edit/{id}', 'ClientController@edit')->name('client.edit');
	Route::post('/update/{id}', 'ClientController@update')->name('client.update');
	Route::get('/delete/{id}', 'ClientController@delete')->name('client.delete');
});
/* Client route end */