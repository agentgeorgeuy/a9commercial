<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SO extends Model
{
    protected $table = 'so_table';
   	protected $fillable = ['so_number', 'pick_date', 'deliver_date', 'driver_id', 'plate_id', 'company_id', 'load_no', 'user_id', 'bags_number', 'payment_id', 'status', 'for_payment', 'client_id',];

   	public function Table()
   	    {   
   	        return DB::select(DB::raw("
   	            SELECT a.id as 'id', a.so_number, a.pick_date, a.deliver_date, a.load_no, a.bags_number, a.status, a.for_payment, b.company_name, c.fname, c.mname, c.lname, d.plate_no, a.client_id
   	            FROM so_table as a
                  LEFT JOIN tracking as b on a.company_id = b.id
                  LEFT JOIN driver as c on a.driver_id = c.id
                  LEFT JOIN plate as d on a.plate_id = d.id
                  LEFT JOIN client as e on a.client_id = e.id
                  ORDER BY a.so_number
   	        "));
   	    }

   	public function UpdateTable($id)
   	    {   
   	        return DB::select(DB::raw("
   	            SELECT a.id as 'id', a.plate_id, a.company_id, a.so_number, a.pick_date, a.deliver_date, a.driver_id, a.load_no, a.bags_number, a.status, a.for_payment, b.company_name, c.fname, c.mname, c.lname, d.plate_no, a.client_id
                  FROM so_table as a
                  LEFT JOIN tracking as b on a.company_id = b.id
                  LEFT JOIN driver as c on a.driver_id = c.id
                  LEFT JOIN plate as d on a.plate_id = d.id
                  LEFT JOIN client as e on a.client_id = e.id
   	              where a.id = $id
   	        "));
   	    }

      public function Partial($id)
          {   
              return DB::select(DB::raw("
                  SELECT a.id as 'id', a.plate_id, a.company_id, a.so_number, a.pick_date, a.deliver_date, a.driver_id, a.load_no, a.bags_number, a.status, a.for_payment, b.company_name, c.fname, c.mname, c.lname, d.plate_no, e.client_name
                  FROM so_table as a
                  LEFT JOIN tracking as b on a.company_id = b.id
                  LEFT JOIN driver as c on a.driver_id = c.id
                  LEFT JOIN plate as d on a.plate_id = d.id
                  LEFT JOIN client as e on a.client_id = e.id
                  where a.id = $id
              "));
          }

}
