<?php

namespace App\Http\Controllers;

use App\SO;
use App\Client;
use App\Tracking;
use App\Plate;
use App\Driver;
use Illuminate\Http\Request;
use App\Http\Requests\SORequest;
use Illuminate\Support\Facades\Auth;
use Response;

class SOController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->track = new Tracking();
        $this->plate = new Plate();
        $this->driver = new Driver();
        $this->sonumber = new SO();
        $this->client = new Client();
    }
    
    public function index()
    {
        $data['rows'] = $this->sonumber->Table();
        return view('sonumbers.index', $data);
    }

    public function create()
    {
        $data['company'] = $this->track->Table();
        $data['client'] = $this->client->Table();
        return view('sonumbers.create', $data);
    }

    public function store(request $request)
    { 
       if($request->company_name == '')
        {
         $response = array(
             'status' => 'fail',
             'msg' => 'Order Failed',
             );
        }
        else
        {
            foreach($request->company_name as $key => $value)
            {   
                $user_id = Auth::user()->id;
                $company = $request['company_val'][$key];
                $driver = $request['driver_val'][$key];
                $plate = $request['plate_val'][$key];
                $so_number = $request['number_so'];
                $client = $request['client_name'][$key];
                $pickup = $request['pickup'][$key];
                $delivered = $request['delivered'][$key];
                $loadnumber = $request['loadnumber'][$key];
                $bagsnumber =   $request['bagsnumber'][$key];
                if(SO::create([
                       'company_id' => $company,
                       'driver_id' => $driver,
                       'plate_id' => $plate,
                       'so_number' => $so_number,
                       'pick_date' => $pickup,
                       'deliver_date' => $delivered,
                       'load_no' => $loadnumber,
                       'user_id' => $user_id,
                       'bags_number' => $bagsnumber,
                       'client_id' => $client
                   ]))
                {
                  $response = array(
                      'status' => 'success',
                      'msg' => 'Order Successfully Saved',
                      );
                }
                else
                {
                  $response = array(
                      'status' => 'fail',
                      'msg' => 'Order Failed',
                      );
                }
            }
        }
        return Response::json( $response );
    }

    public function edit($id)
    {
        $data['value'] = $this->sonumber->UpdateTable($id);
        $data['value'] = $data['value'][0];
        $data['company'] = $this->track->Table();
        return view('sonumbers.edit', $data);
    }

    public function update(request $request, $id)
    {   
        $transaction = SO::find($id);

        if($request->input('so_number') == $transaction->so_number && $request->input('loadnumber') == $transaction->load_no)
        {
            $this->validate($request, [
            'company' => 'required',
            'plate' => 'required',
            'pickup' => 'required',
            'bagsnumber' => 'required',
          ]);
        }
        else
        {
            $this->validate($request, [
            'company' => 'required',
            'plate' => 'required',
            'so_number' => 'required',
            'pickup' => 'required',
            'loadnumber' => 'required',
            'bagsnumber' => 'required',
          ]);
        }
        SO::where('id', '=', $id)
        ->update([
          'company_id' => $request->company,
          'plate_id' => $request->plate,
          'so_number' => $request->so_number,
          'pick_date' => $request->pickup,
          'deliver_date' => $request->delivered,
          'load_no' => $request->loadnumber,
          'user_id' => Auth::user()->id,
          'bags_number' => $request->bagsnumber
        ]);
        
        return redirect()->route('sonumber.index')->with('message', 'Record Successfully Updated!');
    }

    public function delete($id)
    {
      $plates = SO::find($id);
      $plates->delete();

      return redirect()->back()->with('error', 'Record Successfully Removed');
    }

    public function paid($id)
    {
      SO::where('id', '=', $id)->update(['status' => 2]);

      return redirect()->back()->with('success', 'Record Successfully Updated');
    }

    public function unpaid($id)
    {
      SO::where('id', '=', $id)->update(['status' => 1]);

      return redirect()->back()->with('success', 'Record Successfully Updated');
    }

    public function for_payment(Request $request)
    {
      if(empty($request->id)) {
        return response()->json(['status' => 'fail', 'msg' => 'No Record Found']);
      }

      SO::whereIn('id', $request->id)->update(['for_payment' => 1]);

      return response()->json(['status' => 'success', 'msg' => 'Success']);
    }

    public function change_status(request $request, $id)
    { 
        if(empty($request->id)) {
        return redirect()->back()->with('error', 'No Record Found');
      }

      SO::where('id', '=', $request->id)
        ->update([
          'for_payment' => 0
        ]);

      return redirect()->back()->with('success', 'Record Successfully Removed');
    }

    public function get_driver()
    {
        $data['drivers'] = $this->driver->DriverZone();
        return view('sonumbers.get_driver',$data);
    }

    public function get_plate()
    {
        $data['plates'] = $this->plate->PlateZone();
        return view('sonumbers.get_plate',$data);
    }
}
