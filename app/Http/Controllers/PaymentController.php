<?php

namespace App\Http\Controllers;

use App\Partial;
use App\Payment;
use App\SO;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;
use Illuminate\Support\Facades\Auth;
use Response;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->sonumber = new SO();
        $this->payment = new Payment();
    }
    
    public function index()
    {
        $data['rows'] = $this->payment->Table();
        return view('payments.index', $data);
    }

    public function create()
    {   
        $data['rows'] = $this->sonumber->Table();
        return view('payments.create', $data);
    }

    public function store(Request $request)
    {   
        if($request->input('top') == 1)
        {
              $this->validate($request, [
              'top' => 'required',
              'amount' => 'required',
            ]);
        }
        else
        {
            $this->validate($request, [
            'top' => 'required',
            'amount' => 'required',
            'tob' => 'required',
            'check_no' => 'required|min:6',
            'check_date' => 'required',
          ]);
        }
        $data['amount'] = $request->amount;
        $data['top'] = $request->top;
        $data['tob'] = $request->tob;
        $data['dop'] = $request->check_no;
        $data['doc'] = $request->check_date;
        $data['name'] = $request->name;
        $data['user_id'] = Auth::user()->id;

        $values = new Payment($data);
        if($values->save())
        {
            SO::where('for_payment', '=', 1)
              ->update([
                'for_payment' => 0,
                'status' => 3,
                'payment_id' => $values->id
              ]);
        }

        return redirect()->route('payment.index')->with('message', 'Successfully Saved');
    }

    public function partial($id, $so)
    {   
        $data['so_id'] = $id;
        $data['so_num'] = $so;
        $data['rows'] = $this->sonumber->Partial($id);
        return view('payments.partial', $data);
    }

    public function partial_send(request $request, $id, $so)
    {   
        if($request->input('top') == 1)
        {
              $this->validate($request, [
              'top' => 'required',
              'amount' => 'required',
            ]);
        }
        else
        {
            $this->validate($request, [
            'top' => 'required',
            'amount' => 'required',
            'tob' => 'required',
            'check_no' => 'required|min:6',
            'check_date' => 'required',
          ]);
        }
        $data['amount'] = $request->amount;
        $data['so_number'] = $request->number_so;
        $data['top'] = $request->top;
        $data['tob'] = $request->tob;
        $data['check_number'] = $request->check_no;
        $data['check_date'] = $request->check_date;
        $data['payer'] = $request->name;
        $data['user_id'] = Auth::user()->id;

        $values = new Partial($data);
        if($values->save())
        {
            SO::where('id', '=', $id)
              ->update([
                'status' => 2,
              ]);
        }

        return redirect()->route('sonumber.index')->with('message', 'Successfully Saved');
    }

    public function edit()
    {
        
    }

    public function update()
    {

    }

    public function delete($id)
    {
    	$plates = Payment::find($id);
        $plates->delete();

        return redirect()->back()->with('error', 'Record Successfully Removed');
    }
}
