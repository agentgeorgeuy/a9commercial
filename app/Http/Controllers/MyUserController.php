<?php

namespace App\Http\Controllers;

use App\MyUser;
use Illuminate\Http\Request;
use App\Http\Requests\MyUserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MyUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->myuser = new MyUser();
    }
    
    public function index()
    {
        $data['rows'] = $this->myuser->Table();
        return view('users.index', $data);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(MyUserRequest $request)
    {	
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);

        $values = new MyUser($data);
        $values->save();

        return redirect()->route('user.index')->with('message', 'New User Account Registered');
    }

    public function edit($id)
    {
        $data['rows'] = $this->myuser->UpdateTable($id);
        $data['rows'] = $data['rows'][0];

        return view('users.edit', $data);
    }

    public function update(request $request, $id)
    {
        $myuser = MyUser::find($id);

        if($request->input('email') == $myuser->email)
        {
            $this->validate($request, [
                'name' => 'required|min:3',
            ]);
        }
        else
        {
            $this->validate($request, [
                'name' => 'required|min:3',
                'email' => 'required|email|unique:users,email',
            ]);
        }

        $myuser->name = $request->name;
        $myuser->email = $request->email;

        $myuser->save();
        return redirect()->route('user.index')->with('message', 'Account Successfully Updated');
    }

    public function delete($id)
    {
    	$myuser = MyUser::find($id);
        $myuser->delete();

        return redirect()->back()->with('error', 'Account Successfully Removed');
    }




    public function editacc($id)
    {
        $data['rows'] = $this->myuser->UpdateTable($id);
        $data['rows'] = $data['rows'][0];

        return view('users.editacc', $data);
    }

    public function updateacc(request $request, $id)
    {
        $myuser = MyUser::find($id);

        if($request->input('email') == $myuser->email)
        {
            $this->validate($request, [
                'name' => 'required|min:3',
                'password' => 'required|string|min:6|confirmed',
            ]);
        }
        else
        {
            $this->validate($request, [
                'name' => 'required|min:3',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|string|min:6|confirmed',
            ]);
        }
        
            $myuser->name = $request->name;
            $myuser->email = $request->email;
            $myuser->password = Hash::make($request->password);
            $myuser->save();
            return redirect()->back()->with('message', 'Account Successfully Updated');
    }
}
