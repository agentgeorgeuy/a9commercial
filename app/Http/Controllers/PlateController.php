<?php

namespace App\Http\Controllers;

use App\Tracking;
use App\Plate;
use Illuminate\Http\Request;
use App\Http\Requests\PlateRequest;
use Illuminate\Support\Facades\Auth;

class PlateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->track = new Tracking();
        $this->plate = new Plate();
    }
    
    public function index()
    {
        $data['rows'] = $this->plate->Table();
        $data['company'] = $this->track->Table();
        return view('plates.index',$data);
    }

    public function create()
    {
        $data['rows'] = $this->track->Table();
        return view('plates.create',$data);
    }

    public function store(PlateRequest $request)
    {
        $plate = new Plate;

        $plate->company_id = $request->company;
        $plate->plate_no = $request->plate;
        $plate->save();

        return redirect()->route('plate.index')->with('message', 'New Plate No. Registered');
    }

    public function edit($id)
    {
        $data['value'] = $this->plate->UpdateTable($id);
        $data['value'] = $data['value'][0];
        $data['rows'] = $this->track->Table();
        return view('plates.edit', $data);
    }

    public function update(request $request, $id)
    {
        $plates = Plate::find($id);

        if($request->input('plate') == $plates->name)
        {

        }
        else
        {
            $this->validate($request, [
                'plate' => 'required|unique:plate,plate_no',
            ]);
        }

        $plates->plate_no = $request->plate;
        $plates->company_id = $request->company;
        $plates->save();
        return redirect()->route('plate.index')->with('message', 'Record Successfully Updated');
    }

    public function delete($id)
    {
    	$plates = Plate::find($id);
        $plates->delete();

        return redirect()->back()->with('error', 'Plate No. Successfully Removed');
    }
}
