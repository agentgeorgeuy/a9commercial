<?php

namespace App\Http\Controllers;

use App\Tracking;
use Illuminate\Http\Request;
use App\Http\Requests\TrackingRequest;
use Illuminate\Support\Facades\Auth;

class TrackingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->track = new Tracking();
    }
    
    public function index()
    {
        $data['rows'] = $this->track->Table();
        return view('trackings.index', $data);
    }

    public function create()
    {
        return view('trackings.create');
    }

    public function store(TrackingRequest $request)
    {
        $data['company_name'] = $request->name;

        $values = new Tracking($data);
        $values->save();

        return redirect()->route('tracking.index')->with('message', 'New Company Registered');
    }

    public function edit($id)
    {
        $data['rows'] = $this->track->UpdateTable($id);
        $data['rows'] = $data['rows'][0];

        return view('trackings.edit', $data);
    }

    public function update(request $request, $id)
    {
        $tracking = Tracking::find($id);

        if($request->input('name') == $tracking->name)
        {

        }
        else
        {
            $this->validate($request, [
                'name' => 'required|unique:tracking,company_name',
            ]);
        }

        $tracking->company_name = $request->name;

        $tracking->save();
        return redirect()->route('tracking.index')->with('message', 'Record Successfully Updated');
    }

    public function delete($id)
    {
    	$Tracking = Tracking::find($id);
        $Tracking->delete();

        return redirect()->back()->with('error', 'Company Successfully Removed');
    }
}
