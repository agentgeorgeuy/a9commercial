<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->client = new Client();
    }
    
    public function index()
    {
    	$data['rows'] = $this->client->Table();
    	return view('clients.index', $data);
    }

    public function create()
    {
    	return view('clients.create');
    }

    public function store(ClientRequest $request)
    {
    	$client = new Client;
        $client->client_name = $request->name;
        $client->save();

        return redirect()->route('client.index')->with('message', 'New Client Registered');
    }

    public function edit($id)
    {
        $data['rows'] = $this->client->UpdateTable($id);
        $data['rows'] = $data['rows'][0];

        return view('clients.edit', $data);
    }

    public function update(ClientRequest $request, $id)
    {
    	$client = Client::find($id);

    	$client->client_name = $request->name;
    	$client->save();

        return redirect()->route('client.index')->with('message', 'Client Record Updated');

    }

    public function delete($id)
    {
    	$client = Client::find($id);
    	$client->delete();

    	return redirect()->back()->with('error', 'Client has been Removed.');
    }
}
