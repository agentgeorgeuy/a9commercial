<?php

namespace App\Http\Controllers;

use App\Tracking;
use App\Driver;
use Illuminate\Http\Request;
use App\Http\Requests\DriverRequest;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->track = new Tracking();
        $this->driver = new Driver();
    }
    
    public function index()
    {
        $data['rows'] = $this->driver->Table();
        return view('drivers.index', $data);
    }

    public function create()
    {
        $data['rows'] = $this->track->Table();
        return view('drivers.create', $data);
    }

    public function store(request $request)
    {

        $driver = new Driver;
        
        if($request->name.''.$request->mname.''.$request->lname == $driver->fullname)
            {
                return redirect()->back()->with('error', 'Name Already Registered');
            }
            else
            {
                $this->validate($request,[
                'name' => 'required|min:3',
                'mname' => 'required',
                'lname' => 'required|min:2',
                'company' => 'required',
                ]);
            }

        if($request->file('logo') != '')
        {
            if($request->file('logo') && $request->file('logo')->isValid())
                    {

                        $destinationPath = './img/driver_pic/';
                        $filename = time().'.'. $request->file('logo')->getClientOriginalExtension();
                        $request->file('logo')->move($destinationPath, $filename);

                        $driver->logo = $filename;
                    }
                    else
                    {   
                        return redirect()->back()->with('error', 'Invalid Image File');
                    }
        }


        $driver->fname = $request->name;
        $driver->mname = $request->mname;
        $driver->lname = $request->lname;
        $driver->fullname = $request->name.''.$request->mname.''.$request->lname;
        $driver->company_id = $request->company;
        $driver->license_no = $request->license;
        $driver->save();

        return redirect()->route('driver.index')->with('message', 'New Driver Registered');
    }

    public function edit($id)
    {
        $data['track'] = $this->track->Table();
        $data['rows'] = $this->driver->UpdateTable($id);
        $data['rows'] = $data['rows'][0];

        return view('drivers.edit', $data);
    }

    public function update(request $request, $id)
    {
        $driver = Driver::find($id);
        
        if($request->name.''.$request->mname.''.$request->lname == $driver->fullname)
            {
                $this->validate($request,[
                'name' => 'min:6',
                'mname' => 'min:1',
                'lname' => 'min:2',
                'company' => 'required',
                ]);
            }
            else
            {
                $this->validate($request,[
                'name' => 'required|min:6',
                'mname' => 'required',
                'lname' => 'required|min:2',
                'company' => 'required',
                ]);
            }

        if($request->file('logo') != '')
        {   


            if($request->file('logo') && $request->file('logo')->isValid())
                    {
                        if(file_exists(public_path('./img/driver_pic/'.$driver->logo)))
                        {
                            unlink(public_path('./img/driver_pic/'.$driver->logo));
                        }

                        $destinationPath = './img/driver_pic/';
                        $filename = time().'.'. $request->file('logo')->getClientOriginalExtension();
                        $request->file('logo')->move($destinationPath, $filename);

                        $driver->logo = $filename;
                    }
                    else
                    {   
                        return redirect()->back()->with('error', 'Invalid Image File');
                    }
        }


        $driver->fname = $request->name;
        $driver->mname = $request->mname;
        $driver->lname = $request->lname;
        $driver->fullname = $request->name.''.$request->mname.''.$request->lname;
        $driver->company_id = $request->company;
        $driver->license_no = $request->license;
        $driver->save();

        return redirect()->route('driver.index')->with('message', 'Driver Record Updated');
    }

    public function delete($id)
    {
    	$data = Driver::find($id);
        $data->delete();

        return redirect()->back()->with('error', 'Driver has been Removed.');
    }
}
