<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Partial extends Model
{
    protected $table = 'partial_payment';
    protected $fillable = ['amount', 'top', 'tob', 'dop', 'doc', 'name', 'user_id', 'so_number',];
}
