<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Plate extends Model
{
    protected $table = 'plate';
    protected $fillable = ['plate_no', 'company_id',];

    public function Company2()
    {
        return $this->belongsTo('App\Tracking');
    }
    
    public function Table()
        {   
            return DB::select(DB::raw("
                SELECT a.id, a.plate_no, b.company_name
                FROM plate as a
                LEFT JOIN tracking as b on a.company_id = b.id
            "));
        }

    public function UpdateTable($id)
        {   
            return DB::select(DB::raw("
                SELECT a.id, a.plate_no, b.company_name, a.company_id
                FROM plate as a
                LEFT JOIN tracking as b on a.company_id = b.id
                where a.id = $id
            "));
        }

    public function Company()
    {
        return $this->belongsTo('App\Tracking');
    }

    public function PlateZone()
    {
        $category_id = Input::get('id');
        return Plate::where('company_id', $category_id)->get();
    }
}
