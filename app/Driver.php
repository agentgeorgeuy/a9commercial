<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Driver extends Model
{
   	protected $table = 'driver';
   	protected $fillable = ['fname', 'mname', 'lname', 'fullname', 'license_no', 'company_id', 'logo',];

      public function Company()
      {
          return $this->belongsTo('App\Tracking');
      }

   	public function Table()
   	    {   
   	        return DB::select(DB::raw("
   	            SELECT b.fname, b.mname, b.lname, b.id, c.company_name, b.logo, b.license_no
   	            FROM driver as b
                  LEFT JOIN tracking as c on b.company_id = c.id
   	        "));
   	    }

   	public function UpdateTable($id)
   	    {   
   	        return DB::select(DB::raw("
   	            SELECT b.fname, b.mname, b.lname, b.id, c.company_name, b.logo, c.id as cid, b.license_no
                  FROM driver as b
                  LEFT JOIN tracking as c on b.company_id = c.id
   	            where b.id = $id
   	        "));
   	    } 

      public function DriverZone()
      {
          $category_id = Input::get('id');
          return Driver::where('company_id', $category_id)->get();
      }
}
