<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = ['amount', 'top', 'tob', 'dop', 'doc', 'name', 'user_id',];
    protected $hidden = ['foo', 'bar',];

    public function Table()
        {   
            return DB::select(DB::raw("
                SELECT a.id, a.amount, a.top, a.top, a.tob, a.dop, a.doc, a.name, a.user_id, a.created_at, b.so_number, b.status, c.name as 'cname'
                FROM payments as a
                LEFT JOIN so_table as b on a.id = b.payment_id
                LEFT JOIN users as c on a.user_id = c.id
            "));
        }

}
