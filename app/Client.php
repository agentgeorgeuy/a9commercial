<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    protected $table = 'client';
    protected $fillable = ['fname', 'mname', 'lname', 'fullname',];

    public function Table()
        {   
            return DB::select(DB::raw("
                SELECT *
                FROM client as b
            "));
        }

    public function UpdateTable($id)
        {   
            return DB::select(DB::raw("
                SELECT *
                FROM client as b
                where id = $id
            "));
        } 
}
