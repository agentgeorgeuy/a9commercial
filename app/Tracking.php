<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tracking extends Model
{
    protected $table = 'tracking';
    protected $fillable = ['company_name',];

    public function Table()
        {   
            return DB::select(DB::raw("
                SELECT *
                FROM tracking as b
                Order By company_name
            "));
        }

    public function UpdateTable($id)
        {   
            return DB::select(DB::raw("
                SELECT *
                FROM tracking as b
                where id = $id
            "));
        } 
}
