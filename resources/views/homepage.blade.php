@extends('layouts.app')
<style>
	.parallax {
                /* The image used */
                background-image: url('img/parallax1.jpg');

                /* Full height */
                height: 50%; 
                width: 100%;
                /* Create the parallax scrolling effect */
                background-attachment: fixed;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }
</style>
@section('content')
	<div style="margin-top: 30px;"></div>
	<div class="parallax">
		<div class="container-fluid">
			<div class="text-center" style="font-size: 42px; color: #079486;">
				<strong>A9 Commercial - Official Website</strong>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="text-center" style="color: #333; padding-top: 50px;">
			<strong style="font-size: 42px;">OUR PRODUCTS</strong><br>
				
				
				
				

		</div>
		<div class="row" style="padding-top: 10px; padding-bottom: ">
			<div class="col-3" align="center">
				Steel Bar
			</div>
			<div class="col-3" align="center">
				Plywood
			</div>
			<div class="col-3" align="center">
				Plain Sheet
			</div>
			<div class="col-3" align="center">
				galvanized iron sheets
			</div>
		</div>
	</div>

	<div class="parallax"></div>

	<div class="container-fluid">
		<div class="text-center" style="font-size: 42px; color: #333; margin-top: 50px;">
			<strong>OUR TEAM</strong>

		</div>
		<div class="row">
			<div class="card-deck">
			  <div class="card" >
			    <img class="card-img-top" src="..." alt="Card image cap">
			    <div class="card-body">
			      <h5 class="card-title">Card title</h5>
			      <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
			      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
			    </div>
			  </div>
			  <div class="card">
			    <img class="card-img-top" src="..." alt="Card image cap">
			    <div class="card-body">
			      <h5 class="card-title">Card title</h5>
			      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
			      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
			    </div>
			  </div>
			  <div class="card">
			    <img class="card-img-top" src="..." alt="Card image cap">
			    <div class="card-body">
			      <h5 class="card-title">Card title</h5>
			      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
			      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
			    </div>
			  </div>
			</div>
			<div class="col-4" align="center">
				1st
			</div>
			<div class="col-4" align="center">
				2nd
			</div>
			<div class="col-4" align="center">
				3rd
			</div>
		</div>
	</div>

	<

	<footer style="background-color: #454746; padding-top: 30px;">
		<div class="row">
			<div class="col-6" align="center">
				<a href="#" style="color: #fff; margin-right: 15px; font-size: 20px;">PRODUCTS</a>
				<a href="#" style="color: #fff; margin-right: 15px; font-size: 20px;">OUR TEAM</a>
			</div>
			<div class="col-6" align="center">
				<div style=" font-size: 20px; color: #fff;">All copyrights reserved © 2018 - Designed & Developed by <a href="#">George Uy</a></div>
			</div>
		</div>
	</footer>
@endsection