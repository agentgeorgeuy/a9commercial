<!DOCTYPE html>
<html lang="en">
  @include('partials._head')
<body class="bg-dark">
  <div class="container">
    <div class="row">
      @yield('authcontent')
    </div>
  </div>
  @include('partials._script')
</body>
</html>

