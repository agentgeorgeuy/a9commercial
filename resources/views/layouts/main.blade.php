<!DOCTYPE html>
<html lang="en">
	@include('partials._head')
<body>
	@include('partials._nav')
	
		@include('partials._errors')
			@yield("content")

		@include('partials._footer')

	@include('partials._script')
	@yield("script")
</body>
</html>