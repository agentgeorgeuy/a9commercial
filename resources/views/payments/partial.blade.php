@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active"><a href="{{ route('sonumber.index') }}">View SO Transactions</a></li>
	    <li class="breadcrumb-item active">Create Payment</li>
	  </ol>


		<div class="card mb-3">
			<div class="card-header">
				<div class="card-body">
					<div class="row">
				  		<div class="col-12">
				    		<form action="{{ route('payment.partial_send', [$so_id, $so_num]) }}" method="post" role="form" autocomplete="off">
				    			{{ csrf_field() }}
				    			<input type="hidden" name="number_so" value="{{$so_num}}">
				    			<input type="hidden" name="so_id" value="{{$so_id}}">
				    			<div class="form-group">
				    				<div class="form-row">

				    					<div class="col-md-4 {{ $errors->has('top') ? 'has-error' : '' }}">
				    					   <label for="top">Type of Payment</label>
				    					   <select class="form-control" id="top" name="top">
			    								<option value="">Select Mode</option>
			    								<option value="1">Cash</option>
			    								<option value="2">Check</option>
			    							</select>
				    					   @if ($errors->has('top'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">Type of Payment is Required</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					 <div class="col-md-4 {{ $errors->has('amount') ? 'has-error' : '' }}">
				    					   <label for="amount">Amount</label>
				    					    <input class="form-control" id="amount" name="amount" type="text" placeholder="Amount" value="{{ old('amount') }}">
				    					    
				    					    @if ($errors->has('amount'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">Amount is Required</strong>
				    					        </span>
				    					    @endif
				    					 </div>

				    					 <div class="col-md-4 {{ $errors->has('name') ? 'has-error' : '' }}">
				    					   <label for="name">Name.</label>
				    					    <input class="form-control" id="name" name="name" type="text" placeholder="Payer Name" value="{{ old('name') }}">
				    					    
				    					    @if ($errors->has('name'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">Name is Required</strong>
				    					        </span>
				    					    @endif
				    					 </div>

				    					 <div class="col-md-4 {{ $errors->has('tob') ? 'has-error' : '' }}">
				    					   <label for="tob">Type of Bank</label>
				    					   <select class="form-control" id="tob" name="tob">
			    								<option value="">Select Bank</option>
			    								<option value="BDO">BDO</option>
									<option value="BPI">BPI</option>
									<option value="Landbank">Landbank</option>
									<option value="PNB">PNB</option>
									<option value="DBP">DBP</option>
									<option value="Security Bank">Security Bank</option>
									<option value="Chinabank">Chinabank</option>
									<option value="RCBC">RCBC</option>
									<option value="UnionBank">UnionBank</option>
									<option value="citibank">citibank</option>
									<option value="UCPB">UCPB</option>
									<option value="Eastwest Bank">Eastwest Bank</option>
									<option value="HSBC">HSBC</option>
									<option value="Philtrust">Bank</option>
									<option value="AUB">AUB</option>
									<option value="Bank of Commerce">Banks of Commerce</option>
									<option value="Maybank Philippines Inc.">Maybank Philippines Inc.</option>
									<option value="PBCom">PBcom</option>
									<option value="Standard Chartered Bank Philippines">Standard Chartered Bank Philippines</option>
									<option value="Metrobank">Metrobank</option>
									<option value="The Bank of Tokyo-Mitsubishi UFJ, Ltd.">The Bank of Tokyo-Mitsubishi UFJ, Ltd.</option>
									<option value="PVB">PVB</option>
									<option value="Robinsons Bank Corporation">Robinsons Bank Corporation</option>
									<option value="Deutsche Bank">Deutsche Bank</option>
									<option value="ANZ">ANZ</option>
									<option value="Mizuho Bank, Ltd. Manila Branch">Mizuho Bank, Ltd. Manila Branch</option>
									<option value="ING Group N.V.">ING Group N.V.</option>
									<option value="CTBC Bank">CTBC Bank</option>
									<option value="Bank of China Manila Branch">Bank of China Manila Branch</option>
									<option value="Bank of America, N.A.">Bank of America, N.A.</option>
									<option value="Mega International Commercial Bank">Mega International Commercial Bank</option>
									<option value="Korea Exchange Bank">Korea Exchange Bank</option>
									<option value="Bangkok Bank">Bangkok Bank</option>
									<option value="Al-Amanah Islamic Investment Bank of the Philippines">Al-Amanah Islamic Investment Bank of the Philippines</option>
			    							</select>
				    					   @if ($errors->has('tob'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">Type of Bank is Required</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					 <div class="col-md-4 {{ $errors->has('check_no') ? 'has-error' : '' }}">
				    					   <label for="check_no">Check No.</label>
				    					    <input class="form-control" id="check_no" name="check_no" type="text" placeholder="Check #" value="{{ old('check_no') }}">
				    					    
				    					    @if ($errors->has('check_no'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">Check # is Required</strong>
				    					        </span>
				    					    @endif
				    					 </div>

				    					 <div class="col-md-4 {{ $errors->has('check_date') ? 'has-error' : '' }}">
				    					   <label for="check_date">Check Date</label>
				    					    <input class="form-control" id="check_date" name="check_date" type="text" placeholder="Check Date" value="{{ old('check_date') }}">
				    					    
				    					    @if ($errors->has('check_date'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">Check Date  is Required</strong>
				    					        </span>
				    					    @endif
				    					 </div>

				    				</div>
				    			</div>
				    			<button type="submit" class="btn btn-primary btn-block">Save</button>
				    		</form>
				    	</div>
				    </div>

				</div>
				    	<div class="table-responsive">
				    		<form id="business" autocomplete="off">
				    	  	<meta name="csrf-token" content="{{ csrf_token() }}">
				    	  <table class="table table-bordered" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>SO #</th>
										<th>Load #</th>
										<th>Bags</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>SO #</th>
										<th>Load #</th>
										<th>Bags</th>
									</tr>
								</tfoot>
								<tbody>
										@if(count($rows) > 0)
											@foreach($rows as $row)
											<tr>
												<td>{{ $row->so_number }}</td>
												<td>{{ $row->load_no }}</td>
												<td>{{ $row->bags_number }}</td>
												</td>
											</tr>
											@endforeach
										@endif
								</tbody>
				    	  </table>
				    	</div>
				    </form>
			</div>
		</div>
</div>
@endsection
