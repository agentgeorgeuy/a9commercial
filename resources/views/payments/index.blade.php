@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active">View Payment Transactions</li>
	  </ol>

	<div class="card mb-3">
	    <div class="card-header">
	    	<div class="row">
	    	  <div class="col-md-4"> <button class="btn btn-secondary" disabled><i class="fa fa-table"></i> Data Table View</button></div>
	    	  <div class="col-md-4 offset-md-4">
				</div>
	    	</div>
	  	</div>

	    <div class="card-body">
	    	<div class="table-responsive">
	    	  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>SO #</th>
							<th>Type of Payment</th>
							<th>Amount</th>
							<th>Type of Bank</th>
							<th>Check #</th>
							<th>Check Date</th>
							<th>Name of Payer</th>
							<th>Created By</th>
							<th>Created Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>SO #</th>
							<th>Type of Payment</th>
							<th>Amount</th>
							<th>Type of Bank</th>
							<th>Check #</th>
							<th>Check Date</th>
							<th>Name of Payer</th>
							<th>Created By</th>
							<th>Created Date</th>
							<th>Action</th>
						</tr>
					</tfoot>
					<tbody>
							@if(count($rows) > 0)
								@foreach($rows as $row)
								<tr>
									<td>{{ $row->so_number }}</td>
									<td>
										@if($row->top == 1)
											Cash
										@else
											Check
										@endif
									</td>
									<td>₱{{ number_format($row->amount) }}</td>
									<td>{{ $row->tob }}</td>
									<td>{{ $row->dop }}</td>
									<td>{{ $row->doc }}</td>
									<td>{{ $row->name }}</td>
									<td>{{ $row->cname }}</td>
									<td>{{ $row->created_at }}</td>
									<td>
									<a href="#"><i class="fa fa-edit"></i></a> |
									<a href="{{ route('payment.delete', $row->id )}}"><i class="fa fa-trash" onclick="return confirm('Are you sure, you wish to proceed?')"></i></a>
									</td>
								</tr>
								@endforeach
							@endif
					</tbody>
	    	  </table>
	    	</div>
	    </div>
	</div>
</div>
@endsection
