@if(count($drivers) > 0)
	<option value="">Choose Driver</option>
	@foreach($drivers as $item)
		<option value="{{$item->id}}"
				data-formattedname="{{$item->fname.' '.$item->lname}}"
		>{{$item->fname.' '.$item->lname }}</option>
	@endforeach
@else
	<option value="0">No Driver Registered</option>
@endif