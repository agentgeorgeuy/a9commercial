@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    @if(auth::user()->role == 1)
	    <li class="breadcrumb-item active"><a href="{{ route('sonumber.index') }}">View Transactions</a></li>
	    @endif
	    <li class="breadcrumb-item active">Create Transaction</li>
	  </ol>


		<div class="card mb-3">
			<div class="card-header">
				<div class="card-body">
					<div class="row">
				  		<div class="col-12">
				    		<form id="business" autocomplete="off">
				    			<meta name="csrf-token" content="{{ csrf_token() }}" />

				    			<div class="form-group">
				    				<div class="form-row">

				    					<div class="col-md-4 {{ $errors->has('company') ? 'has-error' : '' }}">
				    					   <label for="company">Tracking Name</label>
				    					   <select class="form-control" id="company" name="company" onchange="get_driver($(this).val()); get_plate($(this).val());">
			    								<option value="">Choose Company</option>
			    								@if(count($company) > 0)
			    									@foreach($company as $row)
			    										<option value="{{$row->id}}"
			    										data-formattedname="{{$row->company_name}}"
			    														>{{$row->company_name }}</option>
			    									@endforeach
			    								@endif
			    							</select>
				    					   @if ($errors->has('company'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('company') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					<div class="col-md-4 {{ $errors->has('driver') ? 'has-error' : '' }}">
				    					   <label for="driver">Driver Name</label>
				    					   <select class="form-control" id="driver" name="driver">
			    								<option value="">Choose Driver</option>
			    							</select>

			    							<img src="{{ asset('/img/spinner.gif') }}" id="loader" style="position: absolute;right: -9px; top: 9px; display: none;" />

				    					   @if ($errors->has('driver'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('driver') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

			 	    					<div class="col-md-4 {{ $errors->has('plate') ? 'has-error' : '' }}">
			 	    					   <label for="plate">Plate No.</label>
			 	    					   <select class="form-control" id="plate" name="plate">
			     								<option value="">Choose Plate No.</option>
			     							</select>
			 	    					   @if ($errors->has('plate'))
			 	    					       <span class="help-block" role="alert">
			 	    					           <strong class="text-danger">{{ $errors->first('plate') }}</strong>
			 	    					       </span>
			 	    					   @endif
			 	    					 </div>

				    					 <div class="col-md-4 {{ $errors->has('so_number') ? 'has-error' : '' }}">
				    					    <label for="so_number">SO Number</label>
				    					    <input class="form-control" id="so_number" name="so_number" type="text" placeholder="SO Number">
				    					    
				    					    @if ($errors->has('so_number'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">{{ $errors->first('so_number') }}</strong>
				    					        </span>
				    					    @endif
				    					  </div>

				    					  <div class="col-md-4 {{ $errors->has('pickup') ? 'has-error' : '' }}">
				    					    <label for="pickup">Pick Up Date</label>
				    					    <input class="form-control" id="pickup" name="pickup" type="text" placeholder="Date Taken">
				    					    
				    					    @if ($errors->has('pickup'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">{{ $errors->first('pickup') }}</strong>
				    					        </span>
				    					    @endif
				    					  </div>

				    					  	<div class="col-md-4 {{ $errors->has('delivered') ? 'has-error' : '' }}">
					    					    <label for="delivered">Deliver Date</label>
					    					    <input class="form-control" id="delivered" name="delivered" type="text" placeholder="Date Delivered">
					    					    
					    					    @if ($errors->has('delivered'))
					    					        <span class="help-block" role="alert">
					    					            <strong class="text-danger">{{ $errors->first('delivered') }}</strong>
					    					        </span>
					    					    @endif
				    					  </div>
				    				</div>
				    			</div>
				    			<div class="form-group">
				    				<div class="form-row">
                                        <div class="col-4 {{ $errors->has('loadnumber') ? 'has-error' : '' }}">
                                            <label for="loadnumber">Load Number</label>
                                            <input class="form-control" id="loadnumber" name="loadnumber" type="text" placeholder="Load Number" value="">

                                            @if ($errors->has('loadnumber'))
                                            <span class="help-block" role="alert">
                                                <strong class="text-danger">{{ $errors->first('loadnumber') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="col-md-4 {{ $errors->has('client') ? 'has-error' : '' }}">
				    					    <label for="client">Customer Name</label>
				    					    <input class="form-control" id="client" name="client" type="text" placeholder="Client Name">
				    					    
				    					    @if ($errors->has('client'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">{{ $errors->first('client') }}</strong>
				    					        </span>
				    					    @endif
				    					  </div>
                                        <div class="col-4 {{ $errors->has('bagsnumber') ? 'has-error' : '' }}">
                                            <label for="bagsnumber">Bags</label>
                                            <input class="form-control" id="bagsnumber" name="bagsnumber" type="text" placeholder="Bags" value="">

                                            @if ($errors->has('bagsnumber'))
                                            <span class="help-block" role="alert">
                                                <strong class="text-danger">{{ $errors->first('bagsnumber') }}</strong>
                                            </span>
                                            @endif
                                        </div>
				    			    </div>						    
				    			</div>
				    			<hr>
				    			<button type="submit" class="btn btn-primary btn-block">ADD</button>
				    		</form>
				    	</div>
				    </div>
				</div>
			</div>
		</div>

		<div class="card mb-3">
			<div class="card-header">
				<div class="card-body">
					<div class="row">
				  		<div class="col-12">
							<form id="orders">
								{{ csrf_field() }}
								<input type="hidden" name="number_so">
								<div class="table-responsive">
									<table class="table table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>Tracking Name</th>
												<th>Load Number</th>
												<th>Pickup Date</th>
												<th>Deliver Date</th>
												<th>Plate #</th>
                                                <th>Client Name</th>
												<th>Bags</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<button type="submit" class="btn btn-success btn-block">SAVE</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
@endsection

@section('script')

        <script>
        	$(function(){
			$("#pickup").datepicker({
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonth: 1,
				changeYear: true,
				dateFormat: "mm/dd/yy",
				onClose: function(selectedDate){
					$("#delivered").datepicker("option","minDate",selectedDate);
				}
			});

			$("#delivered").datepicker({
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonth: 1,
				changeYear: true,
				dateFormat: "mm/dd/yy",
				onClose: function(selectedDate){
					$("#pickup").datepicker("option","maxDate",selectedDate);
				}
			});
		});

        $(document).on('change', '#so_number', function() {
        	$('input[name=number_so]').val($(this).val())
        })

        function get_driver(id)
        {	
        	$('#loader').show();
            $.post("/sonumber/get_driver",{id:id,_token:"{{ csrf_token() }}"}).done(function(e){
                $('#driver').html(e);
                $('#loader').hide();
            });
        }

        function get_plate(id)
        {	
            $.post("/sonumber/get_plate",{id:id,_token:"{{ csrf_token() }}"}).done(function(e){
                $('#plate').html(e);
            });
        }

        $(document).ready(function(){
	      	//get it if Status key found
	      	if(localStorage.getItem("status"))
	      	{
	          	$.notify({
	              // options
	              message: 'Orders Successfully Saved.' 
	            },{
	              // settings
	              type: 'success',
	              timer: 5000,
	            });
	          	localStorage.clear();
	      	}
	  	});

         $(document).on('submit', 'form#business', function(e) {
                             	e.preventDefault();
                             	var company_val = $('#company').val(),
                             		company_name = $('#company').find(':selected').data('formattedname'),
                                    client_name = $('#client').val(),
                             		driver_val = $('#driver').val(),
                                    driver_name = $('#driver').find(':selected').data('formattedname'),
                             		plate_val = $('#plate').val(),
                             		plate = $('#plate').find(':selected').data('formattedname'),
                             		so_number = $('#number_so').val(),
                             		pickup = $('#pickup').val(),
                             		delivered = $('#delivered').val(),
                             		loadnumber = $('#loadnumber').val(),
                             		bagsnumber = $('#bagsnumber').val();
                             		total = parseFloat(bagsnumber);
                             		
                             	var row = '<tr><td class="company_name">'+company_name+'<input type="hidden" name="company_name[]" value="'+company_name+'"></td><td class="loadnumber">'+loadnumber+'<input type="hidden" name="loadnumber[]" value="'+loadnumber+'"></td><td class="pickup">'+pickup+'<input type="hidden" name="pickup[]" value="'+pickup+'"></td><td class="delivered">'+delivered+'<input type="hidden" name="delivered[]" value="'+delivered+'"></td><td class="plate">'+plate+'<input type="hidden" name="plate[]" value="'+plate+'"></td><td class="client_name">'+client_name+'<input type="hidden" name="client_name[]" value="'+client_name+'"></td><td class="bagsnumber">'+bagsnumber+'<input type="hidden" name="bagsnumber[]" value="'+bagsnumber+'"></td><td><button class="remove btn btn-danger"><i class="fa fa-trash"> Delete</i></button></td><td hidden>'+company_val+'<input type="hidden" name="company_val[]" value="'+company_val+'"></td><td hidden>'+driver_val+'<input type="hidden" name="driver_val[]" value="'+driver_val+'"></td><td hidden>'+plate_val+'<input type="hidden" name="plate_val[]" value="'+plate_val+'"></td></tr>';
                             	
                             	if(company_name == '') {
                                  $.notify({
                                    // options
                                    message: 'Company Name is required.' 
                                  },{
                                    // settings
                                    type: 'danger',
                                    timer: 5000,
                                  });
                                }

                                if(client_name == '') {
                                  $.notify({
                                    // options
                                    message: 'Client Name is required.' 
                                  },{
                                    // settings
                                    type: 'danger',
                                    timer: 5000,
                                  });
                                }

                             	if(driver_name == '') {
                 			      $.notify({
                 			        // options
                 			        message: 'Driver is required.' 
                 			      },{
                 			        // settings
                 			        type: 'danger',
                 			        timer: 5000,
                 			      });
                 			    }

                             	if(plate == 'NULL') {
                 			      $.notify({
                 			        // options
                 			        message: 'Plate # is required.' 
                 			      },{
                 			        // settings
                 			        type: 'danger',
                 			        timer: 5000,
                 			      });
                 			    }

                             	if(so_number == '') {
                 			      $.notify({
                 			        // options
                 			        message: 'SO Number is required.' 
                 			      },{
                 			        // settings
                 			        type: 'danger',
                 			        timer: 5000,
                 			      });
                 			    }

                             	if(pickup == '') {
                 			      $.notify({
                 			        // options
                 			        message: 'Pickup Date is required.' 
                 			      },{
                 			        // settings
                 			        type: 'danger',
                 			        timer: 5000,
                 			      });
                 			    }

                 			    if(delivered == '') {
                 			      $.notify({
                 			        // options
                 			        message: 'Delivered Date is required.' 
                 			      },{
                 			        // settings
                 			        type: 'danger',
                 			        timer: 5000,
                 			      });
                 			    }

                 			    if(loadnumber == '') {
                 			      $.notify({
                 			        // options
                 			        message: 'Load Number is required.' 
                 			      },{
                 			        // settings
                 			        type: 'danger',
                 			        timer: 5000,
                 			      });
                 			    }

                 			    if(bagsnumber == '') {
                 			      $.notify({
                 			        // options
                 			        message: 'Bag Quantity is required.' 
                 			      },{
                 			        // settings
                 			        type: 'danger',
                 			        timer: 5000,
                 			      });
                 			    }

                             	if(loadnumber != '' && bagsnumber != '' && company_name != '' && so_number != '') {
                             		$('table tbody').prepend(row);

                             		document.getElementById("bagsnumber").value = "";
                                    document.getElementById("client").selectedIndex = "0";
                             	}
                             })
            $(document).on('click', '.remove', function(e) {
            	e.preventDefault();

            	$(this).parent().parent().remove();

            	var sum = 0;
            	$('.total').each(function() {
            		sum += Number($(this).children().html())
            	});

            	$('#grandTotal').html($.number(sum, 2));
            })

                        $(document).on('submit', 'form#orders', function(e) {
            	            e.preventDefault();
            	              var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            	              $data = $(this).serialize();
            	              	$.ajax({
            	                type: 'POST',
            	                url: '/sonumbers/store',
            	                data: $data,
            	                dataType: 'json',
            	                success: function(data) {
            	                	console.log(data);
            	                  if(data.status == 'success') {
            	                    localStorage.setItem("status",data.OperationStatus)
            	                    window.location.reload(); 
            	                  } else
            	                   {
            	                   	$.notify({
            	                   	   // options
            	                   	   message: 'Invalid Output (Table Empty)' 
            	                   	 },{
            	                   	   // settings
            	                   	   type: 'danger',
            	                   	   timer: 5000,
            	                   	 });
            	                   	 return;
            	                  	}
            	                }
            	              })
                        })     
        </script>
@endsection