@extends('layouts.main')

@section('content')
<div class="container">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('home') }}">Dashboard</a>
		</li>
		<li class="breadcrumb-item active"><a href="{{ route('sonumber.index') }}">View Brands </a></li>
	    <li class="breadcrumb-item active">Update Brand</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<form action="{{ route('sonumber.update', $value->id) }}" method="post" role="form" autocomplete="off" >
							{{ csrf_field() }}

							<div class="form-group">
			    				<div class="form-row">

			    					<div class="col-md-4 {{ $errors->has('company') ? 'has-error' : '' }}">
			    					   <label for="company">Tracking Name</label>
			    					   <select class="form-control" id="company" name="company" onchange="get_driver($(this).val()); get_plate($(this).val());">
		    								<option value="{{$value->company_id}}">{{$value->company_name}}</option>
		    								@if(count($company) > 0)
		    									@foreach($company as $row)
		    										<option value="{{$row->id}}">{{$row->company_name }}</option>
		    									@endforeach
		    								@endif
		    							</select>
			    					   @if ($errors->has('company'))
			    					       <span class="help-block" role="alert">
			    					           <strong class="text-danger">{{ $errors->first('company') }}</strong>
			    					       </span>
			    					   @endif
			    					 </div>

			    					<div class="col-md-4 {{ $errors->has('driver') ? 'has-error' : '' }}">
			    					   <label for="driver">Driver Name</label>
			    					   <select class="form-control" id="driver" name="driver">
		    								<option value="{{$value->driver_id}}">{{$value->fname.' '.$value->lname}}</option>
		    							</select>

		    							<img src="{{ asset('/img/spinner.gif') }}" id="loader" style="position: absolute;right: -9px; top: 9px; display: none;" />

			    					   @if ($errors->has('driver'))
			    					       <span class="help-block" role="alert">
			    					           <strong class="text-danger">{{ $errors->first('driver') }}</strong>
			    					       </span>
			    					   @endif
			    					 </div>

		 	    					<div class="col-md-4 {{ $errors->has('plate') ? 'has-error' : '' }}">
		 	    					   <label for="plate">Plate No.</label>
		 	    					   <select class="form-control" id="plate" name="plate">
		     								<option value="{{$value->plate_id}}">{{$value->plate_no}}</option>
		     							</select>
		 	    					   @if ($errors->has('plate'))
		 	    					       <span class="help-block" role="alert">
		 	    					           <strong class="text-danger">{{ $errors->first('plate') }}</strong>
		 	    					       </span>
		 	    					   @endif
		 	    					 </div>

			    					 <div class="col-md-4 {{ $errors->has('so_number') ? 'has-error' : '' }}">
			    					    <label for="so_number">SO Number</label>
			    					    <input class="form-control" id="so_number" name="so_number" type="text" placeholder="SO Number" value="{{$value->so_number}}">
			    					    
			    					    @if ($errors->has('so_number'))
			    					        <span class="help-block" role="alert">
			    					            <strong class="text-danger">{{ $errors->first('so_number') }}</strong>
			    					        </span>
			    					    @endif
			    					  </div>

			    					  <div class="col-md-4 {{ $errors->has('pickup') ? 'has-error' : '' }}">
			    					    <label for="pickup">Pick Up Date</label>
			    					    <input class="form-control" id="pickup" name="pickup" type="text" placeholder="Date Taken" value="{{$value->pick_date}}">
			    					    
			    					    @if ($errors->has('pickup'))
			    					        <span class="help-block" role="alert">
			    					            <strong class="text-danger">{{ $errors->first('pickup') }}</strong>
			    					        </span>
			    					    @endif
			    					  </div>

			    					  	<div class="col-md-4 {{ $errors->has('delivered') ? 'has-error' : '' }}">
				    					    <label for="delivered">Deliver Date</label>
				    					    <input class="form-control" id="delivered" name="delivered" type="text" placeholder="Date Delivered" value="{{$value->deliver_date}}">
				    					    
				    					    @if ($errors->has('delivered'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">{{ $errors->first('delivered') }}</strong>
				    					        </span>
				    					    @endif
			    					  </div>
			    				</div>
			    			</div>
			    			<div class="form-group">
			    				<div class="form-row">
			    					  <div class="col-4 {{ $errors->has('loadnumber') ? 'has-error' : '' }}">
                                            <label for="loadnumber">Load Number</label>
                                            <input class="form-control" id="loadnumber" name="loadnumber" type="text" placeholder="Load Number" value="{{$value->load_no}}">

                                            @if ($errors->has('loadnumber'))
                                            <span class="help-block" role="alert">
                                                <strong class="text-danger">{{ $errors->first('loadnumber') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="col-md-4 {{ $errors->has('client') ? 'has-error' : '' }}">
				    					    <label for="client">Customer Name</label>
				    					    <input class="form-control" id="client" name="client" type="text" placeholder="Client Name" value="{{$value->client_id}}">
				    					    
				    					    @if ($errors->has('client'))
				    					        <span class="help-block" role="alert">
				    					            <strong class="text-danger">{{ $errors->first('client') }}</strong>
				    					        </span>
				    					    @endif
				    					  </div>
                                        <div class="col-4 {{ $errors->has('bagsnumber') ? 'has-error' : '' }}">
                                            <label for="bagsnumber">Bags</label>
                                            <input class="form-control" id="bagsnumber" name="bagsnumber" type="text" placeholder="Bags" value="{{$value->bags_number}}">

                                            @if ($errors->has('bagsnumber'))
                                            <span class="help-block" role="alert">
                                                <strong class="text-danger">{{ $errors->first('bagsnumber') }}</strong>
                                            </span>
                                            @endif
                                        </div>
			    			    </div>						    
			    			</div>
			    			<hr>
			    			<button type="submit" class="btn btn-primary btn-block">UPDATE</button>
			    		</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(function(){
			$("#pickup").datepicker({
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonth: 1,
				changeYear: true,
				dateFormat: "dd/mm/yy",
				onClose: function(selectedDate){
					$("#delivered").datepicker("option","minDate",selectedDate);
				}
			});

			$("#delivered").datepicker({
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonth: 1,
				changeYear: true,
				dateFormat: "dd/mm/yy",
				onClose: function(selectedDate){
					$("#pickup").datepicker("option","maxDate",selectedDate);
				}
			});
		});

		function get_driver(id)
		{	
			$('#loader').show();
		    $.post("/sonumber/get_driver",{id:id,_token:"{{ csrf_token() }}"}).done(function(e){
		        $('#driver').html(e);
		        $('#loader').hide();
		    });
		}

		function get_plate(id)
		{	
		    $.post("/sonumber/get_plate",{id:id,_token:"{{ csrf_token() }}"}).done(function(e){
		        $('#plate').html(e);
		    });
		}

	</script>
@endsection