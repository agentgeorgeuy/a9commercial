@if(count($plates) > 0)
	<option value="0">Choose Plate No.</option>
	@foreach($plates as $item)
		<option value="{{$item->id}}"
				data-formattedname="{{$item->plate_no}}"
				>{{$item->plate_no}}</option>
	@endforeach
@else
	<option value="0"
			data-formattedname="no plate"
	>No Plate # Registered</option>
@endif