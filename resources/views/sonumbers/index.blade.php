@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active">View SO Transactions</li>
	  </ol>

	<div class="card mb-3">
	    <div class="card-header">
	    	<div class="row">
	    	  <div class="col-md-4"> <button class="btn btn-secondary" disabled><i class="fa fa-table"></i> Data Table View</button></div>
	    	  <div class="col-md-4 offset-md-4">
		    	  	<div class="col-sm-4">
						<a href="{{ route('sonumber.create') }}">
							<button class="btn btn-primary" >SO Number &nbsp; <i class="fa fa-plus"></i></button>
						</a>
					</div>
				</div>
	    	</div>
	  	</div>

	    <div class="card-body">
	    	<div class="table-responsive">
	    		<form id="business" autocomplete="off">
	    	  	<meta name="csrf-token" content="{{ csrf_token() }}">
	    	  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>SO #</th>
							<th>Load #</th>
							<th>Tracking</th>
							<th>Pick Up</th>
							<th>Delivered</th>
							<th>Plate #</th>
							<th>Client Name</th>
							<th>Bags</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>SO #</th>
							<th>Load #</th>
							<th>Tracking</th>
							<th>Pick Up</th>
							<th>Delivered</th>
							<th>Plate #</th>
							<th>Client Name</th>
							<th>Bags</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</tfoot>
					<tbody>

						@if(count($rows)> 0)
							@foreach($rows as $row)
								@if($row->for_payment == 0)
								<tr>
									<td>{{ $row->so_number }}</td>
									<td>{{ $row->load_no }}</td>
									<td>{{ $row->company_name }}</td>
									<td>{{ $row->pick_date }}</td>
									<td>{{ $row->deliver_date }}</td>
									<td>{{ $row->plate_no }}</td>
									<td bgcolor="#6F6B65" style="color:#07FCD5">{{ $row->client_id }}</td>
									<td>{{ $row->bags_number }}</td>
									<td @if ($row->status == "1")
											{
												bgcolor="#FF615C"
											}
											@else
											{
												bgcolor="#4BF414"	
											}
											@endif
									>
										@if($row->status == 1)
											<i class="fa fa-frown-o"></i>Unpaid
										@else
											<a href="#"><i class="fa fa-child"></i>Paid</a>
										@endif
									</td>
									<td>
									<a href="{{ route('sonumber.edit', $row->id) }}"><i class="fa fa-edit"" style="margin-left: 5px;"></i></a> |
									<a href="{{ route('sonumber.delete', $row->id) }}"> <i class="fa fa-trash" onclick="return confirm('Are you sure, you wish to proceed?')"></i></a> |
									@if ($row->status != 2)
									<a href="{{ route('sonumber.paid', $row->id) }}"><i class="fa fa-check-circle-o text-success" onclick="return confirm('Are you sure, This is Paid?')"></i></a>
									@else 
									<a href="{{ route('sonumber.unpaid', $row->id) }}"><i class="fa fa-exclamation-circle text-danger" onclick="return confirm('Are you sure, you wish to proceed?')"></i></a></a>
									@endif
									</td>
								</tr>
								@endif
							@endforeach
						@endif

					</tbody>
	    	  </table>
	    	</div>
	    	</form>
	    </div>
	</div>
</div>
@endsection

@section('script')
	<script>
        $(document).ready(function(){
	      	//get it if Status key found
	      	if(localStorage.getItem("status"))
	      	{
	          	$.notify({
	              // options
	              message: 'Records has been Verified' 
	            },{
	              // settings
	              type: 'success',
	              timer: 2500,
	            });
	          	localStorage.clear();
	      	}
	  	});
	</script>
@endsection