<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="#">
      A9 COMMERCIAL
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">

      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('home') }}">
            <i class="fa fa-home"></i>
            <span class="nav-link-text">Home</span>
          </a>
        </li>
        @if(auth::user()->role == 1)
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Accounts">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-user"></i>
            <span class="nav-link-text">Accounts</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li><a href="{{route('user.index')}}"> <i class="fa fa-users"></i> Users</a></li>
          </ul>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Operations">
          <a class="nav-link" href="{{ route('sonumber.index') }}">
            <i class="fa fa-cogs"></i>
             <span class="nav-link-text">SO Transactions</span>
          </a>
        </li>
        @endif
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Accounts">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents1" data-parent="#exampleAccordion">
            <i class="fa fa-user"></i>
            <span class="nav-link-text">Tracking</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents1">
            <li><a href="{{ route('tracking.index') }}"> <i class="fa fa-users"></i> Companies</a></li>
            <li><a href="{{ route('driver.index') }}"> <i class="fa fa-users"></i> Drivers</a></li>
            <li><a href="{{ route('plate.index') }}"> <i class="fa fa-users"></i> Plates</a></li>
          </ul>
        </li>

        @if(auth::user()->role == 2)
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Operations">
          <a class="nav-link" href="{{ route('sonumber.create') }}">
            <i class="fa fa-cogs"></i>
             <span class="nav-link-text">SO Transactions</span>
          </a>
        </li>
        @endif
      </ul>

      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
               <i class="fa fa-user-circle-o"> {{ Auth::user()->name }}</i> <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('user.editacc', auth::user()->id )}}">Settings</a>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>


                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container-fluid">