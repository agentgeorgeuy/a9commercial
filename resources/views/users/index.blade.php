@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active">User Accounts</li>
	  </ol>

	<div class="card mb-3">
	    <div class="card-header">
	    	<div class="row">
	    	  <div class="col-md-4"> <button class="btn btn-secondary" disabled><i class="fa fa-table"></i> Data Table View</button></div>
	    	  <div class="col-md-4 offset-md-4">
		    	  	<div class="col-sm-4">
						<a href="{{ route('user.create') }}">
							<button class="btn btn-primary" >Account &nbsp; <i class="fa fa-plus"></i></button>
						</a>
					</div>
				</div>
	    	</div>
	  	</div>

	    <div class="card-body">
	    	<div class="table-responsive">
	    	  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Action</th>
						</tr>
					</tfoot>
					<tbody>

							@if(count($rows) > 0)
								@foreach($rows as $row)
									@if($row->id != auth::user()->id)
								<tr>
									<td>{{ $row->name }}</td>
									<td>{{ $row->email }}</td>
									<td>
									<a href="{{ route('user.edit', $row->id )}}"><i class="fa fa-edit"></i></a> |
									<a href="{{ route('user.delete', $row->id )}}""><i class="fa fa-trash" onclick="return confirm('Are you sure, you wish to proceed?')"></i></a>
									</td>
								</tr>
									@endif
								@endforeach
							@endif

					</tbody>
	    	  </table>
	    	</div>
	    </div>
	</div>
</div>
@endsection
