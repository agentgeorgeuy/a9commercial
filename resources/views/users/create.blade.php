@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active"><a href="{{ route('user.index') }}">View Users</a></li>
	    <li class="breadcrumb-item active">Create User Account</li>
	  </ol>


		<div class="card mb-3">
			<div class="card-header">
				<div class="card-body">
					<div class="row">
				  		<div class="col-12">
				    		<form action="{{ route('user.store') }}" method="post" role="form" autocomplete="off">
				    			{{ csrf_field() }}

				    			<div class="form-group">
				    				<div class="form-row">

				    					<div class="col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
				    					   <label for="name">Name</label>
				    					   <input class="form-control" id="name" name="name" type="text" placeholder="Input Name" value="{{ old('name') }}">
				    					   @if ($errors->has('name'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('name') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					 <div class="col-md-6 {{ $errors->has('email') ? 'has-error' : '' }}">
				    					 <label for="email">{{ __('E-Mail Address') }}</label>

				    					     <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

				    					     @if ($errors->has('email'))
				    					         <span class="invalid-feedback" role="alert">
				    					             <strong>{{ $errors->first('email') }}</strong>
				    					         </span>
				    					     @endif
				    					 </div>

				    					 <div class="col-md-6 {{ $errors->has('password') ? 'has-error' : '' }}">
				    					     <label for="password">{{ __('Password') }}</label>
				    					         <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

				    					         @if ($errors->has('password'))
				    					             <span class="invalid-feedback" role="alert">
				    					                 <strong>{{ $errors->first('password') }}</strong>
				    					             </span>
				    					         @endif
				    					 </div>

				    					 <div class="col-md-6 {{ $errors->has('password-confirmation') ? 'has-error' : '' }}">
				    					     <label for="password-confirm">{{ __('Confirm Password') }}</label>
				    					         <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
				    					 </div>

				    				</div>
				    			</div>
				    			<button type="submit" class="btn btn-primary btn-block">Save</button>
				    		</form>
				    	</div>
				    </div>
				</div>
			</div>
		</div>
</div>
@endsection
