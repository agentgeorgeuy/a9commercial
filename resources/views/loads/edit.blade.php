@extends('layouts.main')

@section('content')
<div class="container">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('home') }}">Dashboard</a>
		</li>
		<li class="breadcrumb-item active"><a href="{{ route('brand.index') }}">View Brands </a></li>
	    <li class="breadcrumb-item active">Update Brand</li>
	</ol>

	<div class="card mb-3">
		<div class="card-header">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<form action="{{ route('brand.update', $brand->id) }}" method="post" role="form" autocomplete="off">
							{{ csrf_field() }}

							<div class="form-group">
								<div class="form-row">
									<div class="col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
									   <label for="name">Branch Name</label>
									   <input class="form-control" id="name" name="name" type="text" placeholder="Input Branch Name" value="{{ $brand->name }}">
									   @if ($errors->has('name'))
									       <span class="help-block" role="alert">
									           <strong class="text-danger">{{ $errors->first('name') }}</strong>
									       </span>
									   @endif
									 </div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-block">Update</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
