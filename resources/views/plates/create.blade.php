@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active"><a href="{{ route('plate.index') }}">Plate No. View </a></li>
	    <li class="breadcrumb-item active">Register Plate No.</li>
	  </ol>


		<div class="card mb-3">
			<div class="card-header">
				<div class="card-body">
					<div class="row">
				  		<div class="col-12">
				    		<form action="{{ route('plate.store') }}" method="post" role="form" autocomplete="off">
				    			{{ csrf_field() }}

				    			<div class="form-group">
				    				<div class="form-row">

				    					 <div class="col-md-6 {{ $errors->has('company') ? 'has-error' : '' }}">
				    					   <label for="company">Company Name</label>
					    					   <select class="form-control" id="company" name="company">
					    								<option value="">Choose Category</option>
					    								@if(count($rows) > 0)
					    									@foreach($rows as $row)
					    										<option value="{{$row->id}}">{{$row->company_name }}</option>
					    									@endforeach
					    								@endif
					    						</select>
				    					   @if ($errors->has('company'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('company') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					<div class="col-md-6 {{ $errors->has('plate') ? 'has-error' : '' }}">
				    					   <label for="plate">Plate No.</label>
				    					   <input class="form-control" id="plate" name="plate" type="text" placeholder="Input plate No." value="{{ old('plate') }}">
				    					   @if ($errors->has('plate'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('plate') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    				</div>
				    			</div>
				    			<button type="submit" class="btn btn-primary btn-block">Save</button>
				    		</form>
				    	</div>
				    </div>
				</div>
			</div>
		</div>
</div>
@endsection
