@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active"><a href="{{ route('driver.index') }}">View Drivers</a></li>
	    <li class="breadcrumb-item active">Register Driver</li>
	  </ol>


		<div class="card mb-3">
			<div class="card-header">
				<div class="card-body">
					<div class="row">
				  		<div class="col-12">
				    		<form action="{{ route('driver.update', $rows->id) }}" method="post" role="form" enctype="multipart/form-data" autocomplete="off">
				    			{{ csrf_field() }}

				    			<div class="form-group">
				    				<div class="form-row">

				    					<div class="col-md-4 {{ $errors->has('name') ? 'has-error' : '' }}">
				    					   <label for="name">First Name</label>
				    					   <input class="form-control" id="name" name="name" type="text" placeholder="Input First Name" value="{{ $rows->fname }}">
				    					   @if ($errors->has('name'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('name') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					 <div class="col-md-4 {{ $errors->has('mname') ? 'has-error' : '' }}">
				    					   <label for="mname">Middle Name</label>
				    					   <input class="form-control" id="mname" name="mname" type="text" placeholder="Input Middle Name" value="{{ $rows->mname }}">
				    					   @if ($errors->has('mname'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('mname') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					 <div class="col-md-4 {{ $errors->has('lname') ? 'has-error' : '' }}">
				    					   <label for="lname">Last Name</label>
				    					   <input class="form-control" id="lname" name="lname" type="text" placeholder="Input Last Name" value="{{ $rows->lname }}">
				    					   @if ($errors->has('lname'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('lname') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>
				    					 
				    					 <div class="col-md-6 {{ $errors->has('company') ? 'has-error' : '' }}">
				    					   <label for="company">Company Name</label>
					    					   <select class="form-control" id="company" name="company">
					    								<option value="{{$rows->cid}}">{{$rows->company_name}}</option>
					    								@if(count($track) > 0)
					    									@foreach($track as $row)
					    										@if($row->id != $rows->cid)
					    										<option value="{{$row->id}}">{{$row->company_name }}</option>
					    										@endif
					    									@endforeach
					    								@endif
					    						</select>
				    					   @if ($errors->has('company'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('company') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					 <div class="col-md-6 {{ $errors->has('license') ? 'has-error' : '' }}">
				    					   <label for="license">License No.</label>
				    					   <input class="form-control" id="license" name="license" type="text" placeholder="Input License No." value="{{ $rows->license_no }}">
				    					   @if ($errors->has('license'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('license') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    					 <div class="col-md-6 {{ $errors->has('logo') ? 'has-error' : '' }}">
				    					  	  <label for="logo">Picture</label>
				    					  	
				    					  	  <div class="col-sm-12 ">
				    					  	      <input id="logo" type="file" class="form-control" name="logo" value="{{ old('logo') }}" autofocus>
				    					  	
				    					  	      @if ($errors->has('logo'))
				    					  	          <span class="help-block">
				    					  	              <strong>{{ $errors->first('logo') }}</strong>
				    					  	          </span>
				    					  	      @endif
				    					  	  </div>
				    					  	</div>

				    				</div>
				    			</div>

				    			<button type="submit" class="btn btn-primary btn-block">Save</button>
				    		</form>
				    	</div>
				    </div>
				</div>
			</div>
		</div>
</div>
@endsection
