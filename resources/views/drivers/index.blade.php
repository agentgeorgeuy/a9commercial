@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active">View Drivers</li>
	  </ol>

	<div class="card mb-3">
	    <div class="card-header">
	    	<div class="row">
	    	  <div class="col-md-4"> <button class="btn btn-secondary" disabled><i class="fa fa-table"></i> Data Table View</button></div>
	    	  <div class="col-md-4 offset-md-4">
		    	  	<div class="col-sm-4">
						<a href="{{ route('driver.create') }}">
							<button class="btn btn-primary" >Driver &nbsp; <i class="fa fa-plus"></i></button>
						</a>
					</div>
				</div>
	    	</div>
	  	</div>

	    <div class="card-body">
	    	<div class="table-responsive">
	    	  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Company</th>
							<th>Picture</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Company</th>
							<th>Picture</th>
							<th>Action</th>
						</tr>
					</tfoot>
					<tbody>

						@if(count($rows)> 0)
							@foreach($rows as $row)
								<tr>
									<td align="center">{{ $row->fname.' '.$row->mname.' '.$row->lname }}</td>
									<td align="center">{{ $row->company_name }}</td>
									<td align="center">
										@if($row->logo != null)
										<img src="{{asset('/img/driver_pic/'.$row->logo)}}" height="50px" width="50px">
										@else
										<img src="{{asset('/img/coming-soon.png')}}" height="50px" width="50px">
										@endif
									</td>
									<td align="center">
									<a href="{{ route('driver.edit', $row->id) }}"><i class="fa fa-edit"></i></a> |
									<a href="{{ route('driver.delete', $row->id) }}"><i class="fa fa-trash" onclick="return confirm('Are you sure, you wish to proceed?')"></i></a>
									</td>
								</tr>

							@endforeach
						@endif

					</tbody>
	    	  </table>
	    	</div>
	    </div>
	</div>
</div>
@endsection
