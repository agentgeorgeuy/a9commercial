@extends('layouts.main')

@section('content')
<div class="container">
	  <ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="{{ route('home') }}">Dashboard</a>
	    </li>
	    <li class="breadcrumb-item active"><a href="{{ route('client.index') }}">View Clients</a></li>
	    <li class="breadcrumb-item active">Register Client</li>
	  </ol>


		<div class="card mb-3">
			<div class="card-header">
				<div class="card-body">
					<div class="row">
				  		<div class="col-12">
				    		<form action="{{ route('client.store') }}" method="post" role="form" enctype="multipart/form-data" autocomplete="off">
				    			{{ csrf_field() }}

				    			<div class="form-group">
				    				<div class="form-row">

				    					<div class="col-md-12 {{ $errors->has('name') ? 'has-error' : '' }}">
				    					   <label for="name">Client Name</label>
				    					   <input class="form-control" id="name" name="name" type="text" placeholder="Input Client Name" value="{{ old('name') }}">
				    					   @if ($errors->has('name'))
				    					       <span class="help-block" role="alert">
				    					           <strong class="text-danger">{{ $errors->first('name') }}</strong>
				    					       </span>
				    					   @endif
				    					 </div>

				    				</div>
				    			</div>

				    			<button type="submit" class="btn btn-primary btn-block">Save</button>
				    		</form>
				    	</div>
				    </div>
				</div>
			</div>
		</div>
</div>
@endsection
