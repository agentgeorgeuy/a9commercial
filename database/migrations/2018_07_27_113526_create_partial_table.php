<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partial_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payer')->nullable();
            $table->string('so_number');
            $table->string('amount');
            $table->string('payment_date')->nullable();
            $table->string('check_number')->nullable();
            $table->string('check_date')->nullable();
            $table->string('tob')->nullable();
            $table->integer('top');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partial_payment');
    }
}
