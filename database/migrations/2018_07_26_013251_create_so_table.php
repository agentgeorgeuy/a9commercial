<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('so_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('so_number');
            $table->string('pick_date');
            $table->string('deliver_date');
            $table->integer('driver_id');
            $table->integer('plate_id');
            $table->integer('company_id');
            $table->integer('client_id');
            $table->integer('user_id');
            $table->string('load_no');
            $table->integer('bags_number');
            $table->integer('payment_id')->nullable();
            $table->integer('for_payment')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('so_table');
    }
}
